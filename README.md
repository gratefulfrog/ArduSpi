ArduSpi
=======

SPI-like implemenation to allow multiple Arduinos to communicate on a shared serial tx/rx pair

See the [wiki](https://github.com/gratefulfrog/ArduSpi/wiki) for complete documentation.
